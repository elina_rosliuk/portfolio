const gameBoard = document.querySelector('.game-board');
const gameMessage = document.querySelector('.game-message');

const gameManager = {
    sign: 'x',
    player1: {
        name: 'Player 1',
        score: 0,
        currentPlayer: false
    },
    player2: {
        name: 'Player 2',
        score: 0,
        currentPlayer: false
    },
    getCurrentPlayer: function () {
        return this.player1.currentPlayer ? this.player1 : this.player2;
    }
};

export { gameBoard, gameManager, gameMessage };
