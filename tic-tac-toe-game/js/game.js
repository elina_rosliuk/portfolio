import { gameBoard, gameManager, gameMessage } from './variables.js';

const changeSign = () => {
    gameManager.sign === 'o' ? gameManager.sign = 'x' : gameManager.sign = 'o';
}

const changePlayer = () => {
    const player1Name = document.querySelector('.game-player1');
    const player2Name = document.querySelector('.game-player2');

    if (gameManager.player1.currentPlayer) {
        gameManager.player2.currentPlayer = true;
        gameManager.player1.currentPlayer = false;

        player1Name.classList.toggle('currentPlayer');
        player2Name.classList.toggle('currentPlayer');
    } else {
        gameManager.player1.currentPlayer = true;
        gameManager.player2.currentPlayer = false;

        player1Name.classList.toggle('currentPlayer');
        player2Name.classList.toggle('currentPlayer');
    }
}

const initialPlayer = () => {
    const player1Name = document.querySelector('.game-player1');
    const player2Name = document.querySelector('.game-player2');

    const randomNum = Math.random();

    randomNum < 0.5 ? gameManager.player1.currentPlayer = true : gameManager.player2.currentPlayer = true;

    gameManager.player1.currentPlayer
        ? player1Name.classList.add('currentPlayer')
        : player2Name.classList.add('currentPlayer');
}

const checkWin = () => {
    const winningCombinations = [
        [0, 1, 2],
        [3, 4, 5],
        [6, 7, 8],
        [0, 3, 6],
        [1, 4, 7],
        [2, 5, 8],
        [0, 4, 8],
        [2, 4, 6]
    ];

    return winningCombinations.find((combination) => {
        return combination.every((index) => {
            return [...gameBoard.children][index].classList.contains(`sign-${gameManager.sign}`)
        } );
    });
}

const checkDraw = () => {
    return [...gameBoard.children].every((cell) => {
        return cell.classList.contains('sign-o') || cell.classList.contains('sign-x')
    } );
}

const handleScore = () => {
    const gameScore = document.querySelector('.game-score');
    gameScore.textContent = `${gameManager.player1.score} : ${gameManager.player2.score}`;
}

const highlightCombination = (combination) => {
    combination.forEach(index => document.getElementById(index).classList.add('highlightedCell'));
}

const endGame = (winner = null) => {
    if (winner) {
        gameMessage.textContent = `${winner.name} wins!`;
        winner.score = ++winner.score;

        
    } else {
        gameMessage.textContent = `It's a draw!`;
        if (gameManager.player1.score > 0) {
            gameManager.player1.score = --gameManager.player1.score;
        }

        if (gameManager.player2.score > 0) {
            gameManager.player2.score = --gameManager.player2.score;
        }
    }

    handleScore();
    gameBoard.classList.add('disabled');
}

const handleGame = (event) => {
    const chosenCell = event.target;

    if (chosenCell.className === 'game-board-cell' && ![...chosenCell.children].length) {
        chosenCell.insertAdjacentHTML('afterbegin', `<p class='game-board-sign'>${gameManager.sign}</p>`);
        chosenCell.classList.add(`sign-${gameManager.sign}`);

        const winningCombination = checkWin();

        if (winningCombination) {
            highlightCombination(winningCombination);
            endGame(gameManager.getCurrentPlayer());
            return;
        }

        if (checkDraw()) {
            endGame();
            return;
        }

        changeSign();
        changePlayer();
    }
}

export { initialPlayer, handleGame };
