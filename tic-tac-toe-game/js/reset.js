import { gameManager, gameMessage, gameBoard } from './variables';

const clearCells = () => {
    gameMessage.textContent = '';

    const boardCells = [...document.querySelectorAll('.game-board-cell')];
    boardCells.forEach((cell) => {
        [...cell.children].forEach((child) => child.remove());
        cell.classList.remove('sign-x');
        cell.classList.remove('sign-o');
    });
}

const resetGame = () => {
    gameManager.player1.score = 0;
    gameManager.player2.score = 0;

    const gameScore = document.querySelector('.game-score');
    gameScore.textContent = `0 : 0`;
    clearCells();
}

const handleReset = (event) => {
    if (event.target.id === 'newgame-button') {
        gameMessage.textContent = '';
        clearCells();
    }

    if (event.target.id === 'reset-button') {
        resetGame();
    }

    [...document.querySelectorAll('.game-board-cell')].forEach(cell => cell.classList.remove('highlightedCell'));
    gameBoard.classList.remove('disabled');
}

export default handleReset;
