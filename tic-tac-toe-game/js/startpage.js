import { gameManager } from './variables.js';

const startGame = (event) => {
    event.preventDefault();

    const player1Name = document.querySelector('#start-input-1').value;
    const player2Name = document.querySelector('#start-input-2').value;

    if (player1Name) {
        gameManager.player1.name = player1Name;
    }
    if (player2Name) {
        gameManager.player2.name = player2Name;
    }

    const player1 = document.querySelector('.game-player1');
    const player2 = document.querySelector('.game-player2');

    player1.textContent = gameManager.player1.name;
    player2.textContent = gameManager.player2.name;

    const startPage = document.querySelector('.start');
    const gamePage = document.querySelector('.game');

    startPage.style.display = 'none';
    gamePage.style.display = 'block';
}

export default startGame;
