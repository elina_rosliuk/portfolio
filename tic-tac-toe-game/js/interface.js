import { gameBoard, gameManager } from './variables.js';

const createInterface = () => {
    const gamePage = document.querySelector('.game');
    const playersField = `<p class='game-players'>
                            <span class='game-player1'>${gameManager.player1.name}</span> 
                            vs. 
                            <span class='game-player2'>${gameManager.player2.name}</span>
                        </p>`;
    const score = `<p class='game-score'>0 : 0</p>`;

    gamePage.insertAdjacentHTML('afterbegin', score);
    gamePage.insertAdjacentHTML('afterbegin', playersField);

    for (let i = 0; i < 9; i++) {
        const cell = `<div class='game-board-cell' id='${i}'></div>`;
        gameBoard.insertAdjacentHTML('beforeend', cell);
    }
};

export default createInterface;
