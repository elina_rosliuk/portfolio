import startGame from './startpage.js';
import createInterface from './interface.js';
import { initialPlayer, handleGame } from './game.js';
import handleReset from './reset.js';
import './../styles/main.scss';

const startForm = document.querySelector('.start-form');
startForm.addEventListener('submit', startGame);

createInterface();

initialPlayer();
const gameBoard = document.querySelector('.game-board');
gameBoard.addEventListener('click', handleGame);

const gameButtonBlock = document.querySelector('.game-buttonBlock');
gameButtonBlock.addEventListener('click', handleReset);
