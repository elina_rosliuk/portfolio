import { ChangeEvent, FocusEvent, useState } from 'react';

export const useInput = (initialValue: string) => {
  const [value, setValue] = useState(initialValue);

  const handleChange = (event: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>): void => {
    setValue(event.target.value);
  };

  const handleFocus = (event: FocusEvent<HTMLInputElement | HTMLTextAreaElement>): void => {
    event.target.setSelectionRange(-1, -1);
  };

  const saveItem = (chosenItem: HTMLInputElement, ideasState: any, dispatch: any): void => {
    const newValue = value.trim();
    const data = [...ideasState];
    const id = chosenItem.id.split('_')[1];
    const chosenItemFromData = data.find((item) => +id === +item.id);

    if (!chosenItemFromData) return;

    chosenItem.classList.contains('idea-title')
      ? (chosenItemFromData.title = newValue)
      : (chosenItemFromData.description = newValue);

    dispatch({
      type: 'update',
      payload: data,
    });

    setValue(newValue);
  };

  return {
    value,
    setValue,
    handleChange,
    handleFocus,
    saveItem,
  };
};
