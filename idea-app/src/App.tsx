import React, { useEffect, useReducer, useState } from 'react';
import { IdeaList } from './components/IdeaList/IdeaList';
import { Context } from './context/context';
import reducer from './reducer';
import { data } from './data/data';
import { IData } from './data/data';
import { Header } from './components/Header/Header';

const App: React.FC = () => {
    const dataFromStorage: string | null = localStorage.getItem('ideas');
    const dataToSet: Array<IData> = dataFromStorage === null ? data : JSON.parse(dataFromStorage);

    const [ideasState, dispatch] = useReducer(reducer, dataToSet);
    const [cardColor, setCardColor] = useState<string>('green-bg');

    useEffect(() => {
        localStorage.setItem('ideas', JSON.stringify(ideasState));
    }, [ideasState]);

    return (
        <Context.Provider value={{ ideasState, dispatch, cardColor, setCardColor }}>
            <div className="App" aria-label="Your workplace">
                <Header />
                <IdeaList />
            </div>
        </Context.Provider>
    );
}

export default App;
