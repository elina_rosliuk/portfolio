import { IData } from './data/data';

interface IAction {
  type: 'add' | 'remove' | 'update';
  payload: any;
}

export default function reducer(state: Array<IData>, action: IAction) {
  switch (action.type) {
    case 'add':
      return [
        ...state,
        {
          id: Date.now(),
          title: '',
          description: '',
          bgColor: action.payload.bgColor,
          order: action.payload.order,
        },
      ];

    case 'remove':
      return [...state.filter((item) => +item.id !== +action.payload.id)];

    case 'update':
      return action.payload;

    default:
      return state;
  }
}
