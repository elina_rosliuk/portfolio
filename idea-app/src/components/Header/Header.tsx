import React from 'react';
import './header.styles.scss';

export const Header: React.FC = () => (
    <header className="header">
        <hr className="header-line" />
        <h1 className="header-title">Idea Board</h1>
        <hr className="header-line" />
    </header>
)
