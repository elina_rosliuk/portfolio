import React, { useContext, FocusEvent, MouseEvent, KeyboardEvent } from 'react';
import { Context } from '../../context/context';
import { IData } from '../../data/data';
import { useInput } from '../../helpers/inputHook';
import { TextField } from '../shared/TextField';
import { fieldsData } from '../../data/data';
import './idea.styles.scss';

interface IProps {
    card: IData,
    onDragStart: any,
    onDragLeave: any,
    onDrop: any,
    onDragEnd: any,
    onDragOver: any
}

export const Idea: React.FC<IProps> = ({
    card,
    onDragStart,
    onDragLeave,
    onDragEnd,
    onDragOver,
    onDrop,
}) => {
    const { ideasState, dispatch } = useContext(Context);
    const titleInput = useInput(card.title);
    const descriptionInput = useInput(card.description);

    const handleRemove = (event: MouseEvent<HTMLButtonElement>): void => {
        dispatch({
            type: 'remove',
            payload: (event.target as HTMLButtonElement).parentElement,
        });
    };

    const handleEdit = (chosenItem: HTMLInputElement): void => {
        if (chosenItem.readOnly) {
            chosenItem.readOnly = false;
            chosenItem.focus();
        } else {
            chosenItem.blur();
        }
    };

    const handleInputKeyPress = (event: KeyboardEvent<HTMLInputElement | HTMLTextAreaElement>): void => {
        if (event.key !== 'Enter') return;

        handleEdit(event.target as HTMLInputElement)
    }

    const handleBlur = (event: FocusEvent<HTMLInputElement>): void => {
        event.target.readOnly = true;
        event.target.classList.contains('idea-title')
            ? titleInput.saveItem(event.target, ideasState, dispatch)
            : descriptionInput.saveItem(event.target, ideasState, dispatch);
    };

    const idAsString: string = String(card.id);

    const fields = fieldsData.map(field => (
        <TextField
            key={`${field.className}_${idAsString}`}
            id={`${field.className}_${idAsString}`}
            textarea={field.textarea}
            onChange={field.textarea ? descriptionInput.handleChange : titleInput.handleChange}
            onFocus={descriptionInput.handleFocus}
            onKeyPress={handleInputKeyPress}
            onBlur={handleBlur}
            value={field.textarea ? descriptionInput.value : titleInput.value}
            ariaLabel={card.title}
            onClick={(event: MouseEvent<HTMLInputElement | HTMLTextAreaElement>) => { handleEdit(event.target as HTMLInputElement) }}
            className={field.className}
            placeholder={field.placeholder}
        />
    ))

    return (
        <div
            className={`idea-card ${card.bgColor}`}
            draggable={true}
            id={idAsString}
            aria-label='Sticker'
            onDragStart={onDragStart}
            onDragLeave={onDragLeave}
            onDragEnd={onDragEnd}
            onDragOver={onDragOver}
            onDrop={onDrop}
        >
            <button className="cross" onClick={handleRemove} tabIndex={0} aria-label='Delete the idea' >
                &#10006;
            </button>
            {fields}
        </div>
    );
};
