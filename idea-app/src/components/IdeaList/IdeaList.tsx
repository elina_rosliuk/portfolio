import React, { DragEvent, ReactElement, useContext, useState } from 'react';
import { Context } from '../../context/context';
import { ColorRange } from '../ColorRange/ColorRange';
import { Idea } from '../Idea/Idea';
import { IData } from '../../data/data';
import './ideaList.styles.scss';

export const IdeaList: React.FC = () => {
    const { ideasState, dispatch, cardColor } = useContext(Context);
    const [currentCard, setCurrentCard] = useState<null | IData>(null);

    const handleAddItem = (): void => {
        const order = ideasState[0].order + 1;

        dispatch({
            type: 'add',
            payload: { bgColor: cardColor, order },
        });
    };

    const sortCards = (a: IData, b: IData) => a.order < b.order ? 1 : -1;

    const handleDragStart = (event: DragEvent<HTMLDivElement>, card: IData) => setCurrentCard(card);

    const handleDragLeave = (event: DragEvent<HTMLDivElement>) => {
        // @ts-ignore
        event.target.classList.remove('focusedCard');
    };

    const handleDrop = (event: DragEvent<HTMLDivElement>, card: IData) => {
        event.preventDefault();
        const data = ideasState.map((item: IData) => {
            if (item.id === card.id) {
                return { ...item, order: currentCard?.order };
            }

            if (item.id === currentCard?.id) {
                return { ...item, order: card.order }
            }

            return item;
        })

        dispatch({
            type: 'update',
            payload: data,
        });

        (event.target as HTMLDivElement).classList.remove('focusedCard');
    };

    const handleDragEnd = (event: DragEvent<HTMLDivElement>) => { };

    const handleDragOver = (event: DragEvent<HTMLDivElement>) => {
        event.preventDefault();
        if ((event.target as HTMLDivElement).classList.contains('idea-card')) {
            (event.target as HTMLDivElement).classList.add('focusedCard');
        }
    };

    const ideaList: Array<ReactElement> = ideasState.sort(sortCards).map((item: IData) => {
        return (
            <Idea
                key={item.id}
                card={item}
                onDragStart={(event: DragEvent<HTMLDivElement>) => handleDragStart(event, item)}
                onDragLeave={(event: DragEvent<HTMLDivElement>) => handleDragLeave(event)}
                onDrop={(event: DragEvent<HTMLDivElement>) => handleDrop(event, item)}
                onDragEnd={(event: DragEvent<HTMLDivElement>) => handleDragEnd(event)}
                onDragOver={(event: DragEvent<HTMLDivElement>) => handleDragOver(event)}
            />
        );
    });

    return (
        <div className="board" aria-label="Idea board">
            <div className="buttons-block">
                <button className="add-button" onClick={handleAddItem}>
                    Add Idea
                </button>
                <ColorRange />
            </div>
            <div className="idea-list" aria-label="Your ideas list">{ideaList}</div>
        </div>
    );
};
