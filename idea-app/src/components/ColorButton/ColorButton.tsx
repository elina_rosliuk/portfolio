import React, { useContext } from 'react';
import { Context } from '../../context/context';
import './colorButton.styles.scss';

interface IProps {
    className: string,
    color: string
}

export const ColorButton: React.FC<IProps> = ({ className, color }) => {
    const { cardColor } = useContext(Context);

    const isSelectedColor = className.split(' ')[0] === cardColor;
    const buttonColorLabel = `Choose ${color} color`;

    return (
        <>
            <button
                className={className}
                id={isSelectedColor ? 'active' : ''}
                aria-label={isSelectedColor ? `${buttonColorLabel} - active` : buttonColorLabel}
            />
        </>
    );
};
