import React from 'react';

interface ITextFieldProps {
  id: string;
  ariaLabel?: string;
  textarea?: boolean;
  placeholder?: string;
  className?: string;
  value: string;
  onChange: any;
  onBlur: any;
  onClick: any;
  onKeyPress: any;
  onFocus: any;
}

export const TextField: React.FC<ITextFieldProps> = ({ id, value = '', onKeyPress, onClick, onBlur, onFocus, onChange, ariaLabel = '', textarea = false, placeholder = '', className = '' }) => {
  return (
    <>
      { textarea ? (
        <textarea
          id={id}
          className={`input ${className}`}
          placeholder={placeholder}
          maxLength={110}
          aria-label={ariaLabel || "Describe your idea"}
          onFocus={onFocus}
          onKeyPress={onKeyPress}
          onBlur={onBlur}
          onClick={onClick}
          onChange={onChange}
          value={value}
          readOnly
        />) : (
        <input
          id={id}
          className={`input ${className}`}
          type="text"
          maxLength={21}
          placeholder={placeholder}
          onKeyPress={onKeyPress}
          onFocus={onFocus}
          onClick={onClick}
          aria-label={ariaLabel || "Enter a title"}
          onChange={onChange}
          onBlur={onBlur}
          value={value}
          readOnly
        />)}
    </>
  )
}
