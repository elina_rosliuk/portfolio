import React, { useContext, MouseEvent, ReactElement } from 'react';
import { ColorButton } from '../ColorButton/ColorButton';
import { Context } from '../../context/context';
import './colorRange.styles.scss';

export const ColorRange: React.FC = () => {
    const colors: Array<string> = ['green', 'blue', 'pink', 'yellow'];

    const colorButtonsList: Array<ReactElement> = colors.map((color, index) => (
        <ColorButton className={`${color}-bg color-button`} key={color} color={color} />
    ));

    const { setCardColor } = useContext(Context);

    const handleColorChange = (event: MouseEvent<HTMLDivElement>): void => {
        const chosenButton = (event.target as HTMLButtonElement);

        if (chosenButton.classList.contains('color-button')) {
            setCardColor(chosenButton.classList[0]);

            chosenButton.blur();
        }
    };

    return (
        <div className="color-range" onClick={handleColorChange} aria-label='Choose color of sticker' >
            {colorButtonsList}
        </div>
    );
};
