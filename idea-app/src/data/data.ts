export interface IData {
    id: number;
    title: string;
    description: string;
    bgColor: string;
    order: number;
}

export const data: Array<IData> = [
    {
        id: 1,
        order: 1,
        title: 'Create the idea card',
        description: 'Just press the button above to create an idea card. Choose a color from the pallette.',
        bgColor: 'green-bg',
    },
    {
        id: 2,
        order: 2,
        title: 'Update the idea card',
        description: 'Click on the title or description to edit an idea card.',
        bgColor: 'blue-bg',
    },
];

export const fieldsData = [
    { className: 'idea-title', placeholder: 'Enter your title', textarea: false },
    { className: 'idea-description', placeholder: 'Describe your idea', textarea: true }
];
