function renderGameInterface() {
	const gameBlock = document.querySelector('.game');
	const buttons = ['rock', 'paper', 'scissors'];

	gameBlock.insertAdjacentHTML('beforeend', '<div class=\'game-score\'><p class=\'game-score-text\'>Player: 0 vs. Computer: 0</p></div>');
	gameBlock.insertAdjacentHTML('beforeend', '<div class=\'game-buttons-block\'></div>');
	const buttonsBlock = document.querySelector('.game-buttons-block');

	buttons.forEach((buttonName) => {
		const button = `<div class='game-button'><img class='game-img' id='${buttonName}' src='./img/${buttonName}-icon.svg' alt='${buttonName}-card'></div>`;
		buttonsBlock.insertAdjacentHTML('beforeend', button);
	});

	gameBlock.insertAdjacentHTML('beforeend', '<div class=\'game-message-block\'></div>'); 

	const messageBlock = document.querySelector('.game-message-block');
	messageBlock.insertAdjacentHTML('beforeend', '<p class=\'game-message\'></p>');
	messageBlock.insertAdjacentHTML('beforeend', '<p class=\'game-winner\'></p>');
	gameBlock.insertAdjacentHTML('beforeend', '<div class=\'game-restart\'><a class=\'game-restart-link\' href=\'#\'>Restart</a></div>');
}


export default renderGameInterface;