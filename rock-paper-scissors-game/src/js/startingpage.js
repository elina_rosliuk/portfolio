const startBlock = document.querySelector('.start');

const renderStartPage = () => {
    const greeting = '<h2 class=\'start-greeting\'>Let\'s play the famous kid game - Rock, Paper, Scissors!</h2>';
    const rules = `<p class='start-rules'>Everybody knows the rules:
    <br> - Scissors beats a paper,
    <br> - Paper beats rock,
    <br> - Rock beats scissors.
    <br> - And we play up to three wins!</p>`;

    startBlock.insertAdjacentHTML('beforeend', greeting);
    startBlock.insertAdjacentHTML('beforeend', rules);
    startBlock.insertAdjacentHTML('beforeend', '<button class=\'start-button\'>Let\'s play!</button>');
};

const startGame = () => {
    const gameBlock = document.querySelector('.game');
    startBlock.style.display = 'none';
    gameBlock.style.display = 'block';
    document.body.style.backgroundColor = '#514f4f';
};

export { renderStartPage, startGame };