const scoreManager = {
    round: 1,
    computerwins: 0,
    userWins: 0
};

const getComputerChoice = () => {
    const randomIndex = Math.floor(Math.random() * 3);
    const buttons = ['rock', 'paper', 'scissors'];

    return buttons[randomIndex];
};

const upperFirstLetter = (str) => {
    const result = str.replace(str[0], (letter) => letter.toUpperCase());

    return result;
};

const showWinner = () => {
    const winnerBlock = document.querySelector('.game-winner');
    winnerBlock.style.display = '';
    let result;

    if (scoreManager.computerwins > scoreManager.userWins) {
        result = 'Computer wins.';
    } else if (scoreManager.computerwins < scoreManager.userWins) {
        result = 'You win!';
    } else {
        result = 'It\'s a draw!';
    }

    winnerBlock.textContent = result;
};

const game = (event) => {
    if (!event.target.classList.contains('game-img')) {
        return;
    }

    const userChoice = event.target.id;
    const computerChoice = getComputerChoice();
    const gameMessage = document.querySelector('.game-message');
    
    if (scoreManager.round <= 3) {
        let pairResult;

        switch (userChoice[0] + computerChoice[0]) {
        case 'rs':
        case 'pr':
        case 'sp':
            pairResult = 'You\'ve WON!';
            scoreManager.userWins = ++scoreManager.userWins;
            break;
        case 'ps':
        case 'sr':
        case 'rp':
            pairResult = 'You\'ve LOST!';
            scoreManager.computerwins = ++scoreManager.computerwins;
            break;
        case 'pp':
        case 'ss':
        case 'rr':
            pairResult = 'It\'s a draw!';
            break;
        default: 
            break;
        }

        const messageText = `Round ${scoreManager.round}, ${upperFirstLetter(userChoice)} vs. ${upperFirstLetter(computerChoice)}, ${pairResult}`;
        
        gameMessage.style.display = '';
        gameMessage.textContent = messageText;

        scoreManager.round = ++scoreManager.round;
        const scoreText = document.querySelector('.game-score-text');
        scoreText.textContent = `Player: ${scoreManager.userWins} vs. Computer: ${scoreManager.computerwins}`;
    } 

    if (scoreManager.round === 4) {
        setTimeout(() => gameMessage.style.display = 'none', 2000);
        showWinner();
    }
};

const handleRestart = () => {
    const winnerBlock = document.querySelector('.game-winner');
    const gameMessage = document.querySelector('.game-message');
    
    winnerBlock.style.display = 'none';
    gameMessage.textContent = '';

    scoreManager.round = 1;
    scoreManager.userWins = 0;
    scoreManager.computerwins = 0;

    const scoreText = document.querySelector('.game-score-text');
    scoreText.textContent = 'Player: 0 vs. Computer: 0';
};


export { game, handleRestart } ;