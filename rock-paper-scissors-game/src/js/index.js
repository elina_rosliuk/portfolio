import { renderStartPage, startGame } from './startingpage.js';
import renderGameInterface from './gameinterface.js';
import { game, handleRestart } from './game.js';
import './../styles/index.scss';

renderStartPage();
renderGameInterface();

const startButton = document.querySelector('.start-button');
const buttonsBlock = document.querySelector('.game-buttons-block');
const restartButton = document.querySelector('.game-restart-link');

startButton.addEventListener('click', startGame);
buttonsBlock.addEventListener('click', game);
restartButton.addEventListener('click', handleRestart);
