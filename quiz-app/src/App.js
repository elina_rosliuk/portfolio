import React, { useState } from 'react';
import { Quiz } from './components/Quiz';
import { Context } from './context';
import { FinishPage } from './components/FinishPage';
import questions from './questions.json';
import { Header } from './components/Header';

function getRandomNumber(dataLength, toClear = false) {
    const usedNumbers = [];

    if (toClear) {
        usedNumbers.length = 0;
    }

    const generateNum = () => Math.floor(Math.random() * dataLength);
    let num = generateNum();

    if (usedNumbers.find((item) => +item === +num)) {
        num = generateNum();
    } else {
        usedNumbers.push(num);

        return num;
    }
}

function App() {
    const [isFinished, setGameFinish] = useState(false);
    const [score, setScore] = useState(0);

    const data = questions;

    const [currentNumber, setCurrentNumber] = useState(getRandomNumber(data.length) ?? 0);
    
    const dataToSend = {
        isFinished,
        setGameFinish,
        score,
        setScore,
        currentNumber,
        setCurrentNumber,
        data,
        getRandomNumber,
    };

    return (
        <div className="App">
            <Context.Provider value={dataToSend}>
                <Header />
                <main className="main">{isFinished ? <FinishPage message={isFinished} /> : <Quiz />}</main>
            </Context.Provider>
        </div>
    );
}

export default App;
