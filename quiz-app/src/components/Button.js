import React, { Fragment, useContext } from 'react';
import { Context } from '../context';

export const Button = ({ id, children }) => {
    const { setScore, setGameFinish, setCurrentNumber, data, getRandomNumber } = useContext(Context);
    const dataLength = data.length;

    const handleButtonClick = (event) => {
        if (event.target.id === 'new-game-button') {
            setScore(0);
            setCurrentNumber(getRandomNumber(dataLength), true);
        }

        if (event.target.id === 'skip-game-button') {
            setCurrentNumber(getRandomNumber(dataLength));

            event.target.style.visibility = 'hidden';
        }

        if (event.target.id === 'restart-game-button') {
            setGameFinish(false);
            setScore(0);
            setCurrentNumber(getRandomNumber(dataLength), true);
        }
    };

    return (
        <Fragment>
            <button id={id} className="button" onClick={handleButtonClick}>
                {children}
            </button>
        </Fragment>
    );
};
