import React, { useContext } from 'react';
import { Context } from './../context';
import { Button } from './Button';

export const FinishPage = (props) => {
    const { score } = useContext(Context);
    
    return (
        <div className="finish-block">
            <div className="finish-text-block">
                <h2 className="finish-result">{props.message}</h2>
                <p className="finish-score">Your score: {score} </p>
            </div>
            <Button id="restart-game-button" >
                Restart
            </Button>
        </div>
    );
};
