import React from 'react';

export const Header = () => {
    return (
        <header>
            <h1 className="title">Quiz</h1>
            <hr />
        </header>
    );
};
