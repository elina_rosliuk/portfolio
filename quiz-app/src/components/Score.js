import React, { useContext } from 'react';
import { Context } from './../context';

export const Score = () => {
    const { score } = useContext(Context);

    return (
        <div className="score">
            <p>Current score: {score}</p>
        </div>
    );
};
