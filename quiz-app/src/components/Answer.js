import React from 'react';

export const Answer = ({ id, handleClick, answer }) => {
    return (
        <div className="answer" id={id} onClick={handleClick} >
            {answer}
        </div>
    );
};
