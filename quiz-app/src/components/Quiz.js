import React, { useContext,  Fragment } from 'react';
import { Answer } from './Answer';
import { Score } from './Score';
import { Context } from './../context';
import { Button } from './Button';

export const Quiz = (props) => {
    const { score, setScore, setGameFinish, currentNumber, setCurrentNumber, data, getRandomNumber } = useContext(Context);

    const correctAnswer = data[currentNumber]['correct'];

    const handleClick = (event) => {
        if (event.target.classList.contains('answer') && +event.target.id === +correctAnswer) {
            const newScore = score + 100;

            if (newScore === 1000) {
                setGameFinish("You've reached 1000 points! You are the winner!");
            }

            setScore(newScore);
            setCurrentNumber(getRandomNumber(data.length));
        } else {
            setGameFinish("You've lost.");
        }
    };

    const answerList = data[currentNumber]['content'].map((item, index) => (
        <Answer answer={item} key={index} id={index} handleClick={handleClick} />
    ));


    return (
        <Fragment>
            <div className="qa-block">
                <div className="question-block">
                    <p className="question">{data[currentNumber]['question']}</p>
                </div>
                <div className="answer-block">
                    {answerList}
                </div>
            </div>
            <div className="game-manage-pannel">
                <Button id="new-game-button">
                    New Game
                </Button>
                <Score />
                <Button id="skip-game-button">
                    Skip Question
                </Button>
            </div>
        </Fragment>
    );
};
